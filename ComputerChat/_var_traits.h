// -*- C++ -*-
/**************************** -_var_traits.h- ****************************/
/**		        author: Corbin Matschull                         */
/**			date: 6/25/2015                                  */
/**		        copyright: MIT                                   */
/**                                                                      */
/** *------------------------------------------------------------------* */
/**                                                                      */
/** This file is given to use free of charge and without limitation      */
/** The only agreements you as a user agree to in this file include-     */
/** are and not limited to:                                              */
/** a) Copying for monetary gain or personal profit                      */
/** b) Redistribution without proper attribution                         */
/*************************************************************************/
#ifndef VARTRAITS_H
#define VARTRAITS_H
#include <iosfwd>
#include <vector>

// =======================================================================
// -----------------------------------------------------------------------
//      EXPORT CHARACTERS AND TEMPLATE DEFINITIONS
template<class T, class Xtfwd> class forwardDecl
{
    public:
        // ============================================================
        // ------------------------------------------------------------
        //      STATIC EXPORT DEFINITIONS
#       define      __IMPLICIT_CAST_CONVERSION
#       define      __CONSTANT_CAST_CONVERSION
#       define      __STATIC_CAST_CONVERSION


        // ===========================================================
        // -----------------------------------------------------------
        //      ATOMIC VERIFIERS
        static bool verify_my_atomic(std::vector<long> baseFollower,
                                     bool hasAttributes,
                                     bool attrPassByRef,
                                     int opassed
                                    );

    public:

        // ============================================================ //
        //////////////////////////////////////////////////////////////////
        //                                                              //
        //                                                              //
        //                  STATIC FORWARD DECLARATION                  //
        //                                                              //
        //                                                              //
        //////////////////////////////////////////////////////////////////
        Xtfwd create_static_decl(Xtfwd _unaryD, Xtfwd _unaryY) noexcept;


        // ============================================================ //
        //////////////////////////////////////////////////////////////////
        //                                                              //
        //                                                              //
        //                  CONSTANT FORWARD DECLARATION                //
        //                                                              //
        //                                                              //
        //////////////////////////////////////////////////////////////////
        Xtfwd create_const_decl(Xtfwd _cunaryD, Xtfwd _cunaryY) noexcept;
};



typedef const char __constchar_t, *__pconstchar_t, *__lpconstchar_t;
typedef unsigned char UCHAR;
typedef bool BOOL_T;
typedef int16_t WORD,  *PWORD,  *LPWORD;
typedef int32_t DWORD, *PDWORD, *LPDWORD;
typedef int64_t QWORD, *PQWORD, *LPQWORD;
typedef std::string CSTR, *PCSTR, *LPCSTR;
#endif
