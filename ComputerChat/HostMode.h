#pragma once

#include "hostSocket.h"

#include <string>
#include <vector>

class HostMode
{
    public:
        HostMode();
};

class Ban
{
    public:
        /**
         * \param usr - Username
         * \param roomname - Name of the room the user you want to ban is in
         * \param isPerm - TRUE if the ban is a permaban/FALSE if you want to define a time period for the ban
         * \param bt - Ban time, only use this if <bool>isPerm is false
         */
        Ban(std::string usr,
            std::string roomname,
            bool isPerm,
            int bt);

        Ban sendBanHammer(){ return (*this); }

        virtual ~Ban();

    protected:
        // Number of current users banned
        int usrBanned;

        // Pack this with the names of the users
        char usrArr[];

        // Unique user ID
        // This will be assigned to user upon joining server
        int usrUUID;
    private:
};
